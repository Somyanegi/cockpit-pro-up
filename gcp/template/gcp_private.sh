#!/usr/bin/bash
set -x
export PATH="/google-cloud-sdk/bin:$PATH"
gcloud auth activate-service-account --key-file ./steady-math-394705-653173751100.json
gcloud container clusters get-credentials public-cluster --zone us-central1-f --project steady-math-394705
kubectl create ns gke
kubectl apply -f dep.yaml -n gke
kubectl label ns --all goldilocks.fairwinds.com/enabled=true
helm repo add fairwinds-stable https://charts.fairwinds.com/stable
helm upgrade --install goldilocks fairwinds-stable/goldilocks --namespace goldilocks --create-namespace --set vpa.enabled=true
kubectl create role service-reader --verb=get --verb=list --verb=watch --verb=create --verb=update --verb=delete --resource=services
kubectl create rolebinding service-reader-binding --role=service-reader --user=system:serviceaccount:gitlab-runner:default --namespace=goldilocks
kubectl patch svc goldilocks-dashboard -n goldilocks -p '{"spec": {"type": "LoadBalancer"}}'
