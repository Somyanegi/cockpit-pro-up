#!/bin/bash

# List all load balancer ARNs and store them in a variables
lb_name=$(aws elb describe-load-balancers --region ap-south-1 | jq -r '.LoadBalancerDescriptions[].LoadBalancerName')
# Check if there are no load balancers to deletes
if [[ -z "$lb_name" ]]; then
  echo "No load balancers found to delete."
  exit 0
fi

# Loop through the ARNs and delete each load balancer
for name in $lb_name; do
    aws elb delete-load-balancer --load-balancer-name $name
    echo "Deleting load balancer with Name: $name"

done
echo "All load balancers have been deleted."

lb_test_name=$(aws elb describe-load-balancers --region ap-south-1 | jq -r '.LoadBalancerDescriptions[].LoadBalancerName')
echo $lb_test_name
#  test

