aks_name                = "my-aks-cluster"
dns_prefix              = "myakscluster"
rg_location             = "east us"
rg_name                 = "my-aks-cluster"
node_count              = 1
vnet_1                  = "vnet01"
subnets = [
  "subnet01",
  "subnet02"
]
identity_type           = "SystemAssigned"
default_node_pool_name  = "default"
default_node_vm_size    = "Standard_DS2_v2"
enable_auto_scaling     = "true"
min_node_count          = 1
max_node_count          = 2
availability_zones = ["1", "2", "3"]  
network_plugin          = "kubenet"
dns_service_ip          = "192.168.1.1"
service_cidr            = "192.168.0.0/16"
pod_cidr                = "172.16.0.0/22"
default_node_pool_type  = "VirtualMachineScaleSets"
vnet_1_address_space    = ["172.1.0.0/16"]
subnet_address_prefixes = [
  "172.1.0.0/17",
  "172.1.128.0/17"
]
private_cluster_enabled = true
