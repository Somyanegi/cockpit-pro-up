# Create an AKS cluster
resource "azurerm_kubernetes_cluster" "example" {
  name                = var.aks_name
  location            = var.rg_location
  resource_group_name = var.rg_name
  dns_prefix          = var.dns_prefix
  kubernetes_version  = var.aks_version
  

  identity {
    type = var.identity_type
  }

  default_node_pool {
    name                = var.default_node_pool_name
    node_count          = var.node_count
    vm_size             = var.default_node_vm_size
    vnet_subnet_id      = var.subnet_1_id
    enable_auto_scaling = var.enable_auto_scaling
    min_count           = var.min_node_count
    max_count           = var.max_node_count
    type                = var.default_node_pool_type
     zones = var.availability_zones
  }

  network_profile {
    network_plugin = var.network_plugin
    dns_service_ip = var.dns_service_ip
    service_cidr   = var.service_cidr
    pod_cidr       = var.pod_cidr
  }
  private_cluster_enabled = var.private_cluster_enabled
}
resource "null_resource" "get_kubeconfig" {
  depends_on = [azurerm_kubernetes_cluster.example]
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = "az aks get-credentials --resource-group ${var.rg_name} --name ${var.aks_name} --overwrite-existing --file ./.newkubeconfig.yaml"
  }
}

# resource "azurerm_private_dns_zone_virtual_network_link" "link_vnet" {
#   count                 = var.private_cluster_enabled ? 1 : 0
#   name                  = var.private_dns_link
#   private_dns_zone_name = join(".", slice(split(".", azurerm_kubernetes_cluster.example.private_fqdn), 1, length(split(".", azurerm_kubernetes_cluster.example.private_fqdn))))
#   resource_group_name   = "MC_${azurerm_resource_group.example-2.name}_${azurerm_kubernetes_cluster.example.name}_${azurerm_resource_group.example-2.location}"
#   virtual_network_id    = azurerm_virtual_network.example-2[0].id
# }
