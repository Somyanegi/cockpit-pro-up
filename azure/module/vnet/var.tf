variable "rg_location" {
  type        = string
  default     = "east us"
  description = "The location/region for the Azure resource group."
}

variable "rg_name" {
  type        = string
  default     = "my-aks-cluster"
  description = "The name for the Azure resource group."
}

variable "vnet_1" {
  type        = string
  default     = "vnet01"
  description = "The name of the first virtual network."
}
variable "subnets" {
  type        = list(string)
  default     = ["subnet01", "subnet02"]
  description = "A list of subnet names."
}


variable "vnet_1_address_space" {
  type        = list(string)
  default     = ["172.1.0.0/16"]
  description = "The address space of the first virtual network."
}
variable "subnet_address_prefixes" {
  type        = list(string)
  default     = ["172.1.0.0/17", "172.1.128.0/17"]
  description = "A list of subnet address prefixes."
}


variable "private_cluster_enabled" {
  type        = bool
  default     = true
  description = "Whether the AKS cluster is private."
}